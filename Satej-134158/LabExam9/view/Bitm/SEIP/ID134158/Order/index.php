<?php
session_start();
include_once('../../../../../vendor/autoload.php');

use App\Bitm\SEIP\ID134158\Order\Order;

$obj = new Order();
$obj->setData($_POST);
$data = $obj->index();

?>
<a href="create.php" class="btn btn-success" role="button">New Create</a>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>LabExam9</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Order Form</h2>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>SL</th>
                <td>Id</td>
                <th>Customer Name</th>
                <th>Order_List</th>
                <th>Action</th>

            </tr>
        </thead>
        <tbody>
            <?php
                $count = 0;
                foreach ($data as $row)
                    {
                        $count++;
            ?>
            <tr>
                <td><?php echo $count++; ?></td>
                <td><?php echo $row['id']; ?></td>
                <td><?php echo $row['customer name']; ?></td>
                <td><?php echo $row['order list']; ?></td>

                <td>
                    <a href="view.php?id=<?php echo $row['id'] ?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $row['id'] ?>" class="btn btn-success" role="button">Edit</a>
                    <a href="trash.php?id=<?php echo $row['id'] ?>" class="btn btn-warning" role="button">Trash</a>
                    <a href="delete.php?id=<?php echo $row['id'] ?>" class="btn btn-danger" role="button">Delete</a>
            </tr>

             <?php } ?>
        </tbody>
    </table>


</div>

</body>
</html>
