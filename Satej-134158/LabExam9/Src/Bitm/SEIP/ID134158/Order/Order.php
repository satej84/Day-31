<?php

namespace App\Bitm\SEIP\ID134158\Order;

use PDO;
//session_start();
class Order
{
    public $id="";
    public $customerName="";
    public $orderList="";
//    public $delete_at="1";
    public $serverName="localhost";
    public $databaseName="labexam9";
    public $user="root";
    public $pass="";
    public $con;

    public function __construct()
    {
        try {
            $this->con = new PDO("mysql:host=$this->serverName;dbname=$this->databaseName", $this->user, $this->pass);
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data=array())
    {
        //var_dump($data);

        if(array_key_exists('customerName',$data) and !empty($data)){
            $this->customerName=$data['customerName'];
        }
        if(array_key_exists('oList',$data) and !empty($data)){
            $this->orderList=$data['oList'];

        }
        if(array_key_exists('id',$data) and !empty($data)){
            $this->id=$data['id'];
        }
        return $this;
    }
    public function store()
    {
        $query = "INSERT INTO `order` (`customer name`, `order list`) VALUES (:cName,:oList)";
        $smt = $this->con->prepare($query);
        $result = $smt->execute(array(':cName' => $this->customerName,':oList' => $this->orderList));
        //echo $result;
        if($result)
        {
             echo "Congrats!: Your Data has been input successfully.";
        }else{
            echo"Sorry!: Data has not been inserted in database.";
        }
    }
    public function index(){
        $sql="SELECT * FROM `order`";
        $stm=$this->con->query($sql);
        $dat = $stm->fetchAll(PDO::FETCH_ASSOC);
        return $dat;
    }
    public function view(){

        $sql="SELECT * FROM `order` WHERE `id` =:id";
        $stm=$this->con->prepare($sql);
        $stm->execute(array(':id' => $this->id));
        $singledata=$stm->fetch(PDO::FETCH_ASSOC);
        return $singledata;
    }
    public function update()
    {   $query = "UPDATE `order` SET `customer name`=:ccName,`order list`=:coList WHERE id=:id ";
        $smt = $this->con->prepare($query);
        $result = $smt->execute(array(':ccName' => $this->customerName,':coList' => $this->orderList, ':id'=>$this->id));
        //echo $result;
        if($result)
        {
            echo "Congrats!: Your Data has been Updated successfully.";
        }else{
            echo"Sorry!: Data has not been updated in database.";
        }
    }
    public function Delete()
    {
        $sql="DELETE  FROM `order` WHERE `id` =:id";
        $stm=$this->con->prepare($sql);
        $result = $stm->execute(array(':id' =>$this->id));

        if($result)
        {
            header("Location:index.php");
        }
    }
    public function delete_at()
    {
        echo $this->delete_at='1';

        $query = "UPDATE `order` SET `delete_at`='1' WHERE id=:uid";
        $smt = $this->con->prepare($query);
        $result = $smt->execute(array(':uid'=>$this->id));
        //var_dump($result);
        //echo $result;
        if($result)
        {
            header("Location:index.php");
        }
    }
    public function all_delete_show(){

        $sql="SELECT * FROM student WHERE delete_at='1'";
        $stm=$this->con->query($sql);
        $alldata=$stm->fetchAll(PDO::FETCH_ASSOC);
        return $alldata;
        if(alldata)
        {
            header("Location:all_delete_show.php");
        }
    }
    public function restore()
    {
        $this->delete_at='0';

        $query = "UPDATE `order` SET `delete_at`='0' WHERE id=:uid";
        $smt = $this->con->prepare($query);
        $result = $smt->execute(array(':uid'=>$this->id));
        //var_dump($result);
        //echo $result;
        if($result)
        {
            header("Location:index.php");
        }
    }
}