<?php

session_start();
include_once('../../../../../vendor/autoload.php');
use \App\Bitm\SEIP\ID134158\Order\Order;
$obj = new Order();
$obj->setData($_GET);
$data = $obj->view();
//var_dump($data);

//die();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>LabExam9</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Update Data</h2>
    <form action="update.php" method="post">
        <input type="hidden"name="id" value="<?php echo $data['id']; ?>" >
        <div class="form-group">
            <label>Customer Name:</label>
            <input type="text" class="form-control" name="customerName" value="<?php echo $data['customer name']; ?>">
        </div>
        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList" value="<?php
                    if (in_array("Burger",$data)) {
                        echo "Checked";
                    }
                    ?>">Burger
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList" value="<?php
                    if (in_array("Juice", $data)) {
                        echo "Checked";
                    }
                    ?>">Juice
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList" value="<?php
                    if (in_array("Sandwitch", $data)) {
                        echo "Checked";
                    }
                    ?>" >Sandwitch
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList" value="<?php
                    if (in_array("Salat", $data)) {
                        echo "Checked";
                    }
                    ?>">Salat
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList" value="<?php
                    if (in_array("Pizza", $data)) {
                        echo "Checked";
                    }
                    ?>" >Pizza
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList" value="<?php
                    if (in_array("Coffee", $data)) {
                        echo "Checked";
                    }
                    ?>" >Coffee
                </label>
            </div>
        </div>
        <button type="submit" class="btn btn-default">Update</button>

    </form>
</div>

</body>
</html>

