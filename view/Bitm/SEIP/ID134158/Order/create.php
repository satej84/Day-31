<!DOCTYPE html>
<html lang="en">
<head>
    <title>LabExam9</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Order Information form</h2>
    <form action="store.php" method="post">
        <div class="form-group">
            <label>Customer Name:</label>
            <input type="text" class="form-control" name="customerName">
        </div>
        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList[]" value="Burger">Burger
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList[]" value="Juice">Juice
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList[]" value="Sandwitch" >Sandwitch
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList[]" value="Salat">Salat
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList[]" value="Pizza" >Pizza
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="oList[]" value="Coffee" >Coffee
                </label>
            </div>
        </div>
        <button type="submit" class="btn btn-default">Click to Order</button>
    </form>
</div>

</body>
</html>


